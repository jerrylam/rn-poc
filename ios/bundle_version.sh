#!/bin/sh

#  bundle_version.sh
#  SCMobileMoney
#
#  Created by Kevin Lo on 5/2/2018.
#  Copyright © 2018 Facebook. All rights reserved.

# Version number - CFBundleShortVersionString
VERSION_NUMBER=`ruby -rjson -e 'j = JSON.parse(File.read("../package.json")); puts j["version"]'`
/usr/libexec/PlistBuddy -c \
"Set :CFBundleShortVersionString $VERSION_NUMBER" "${TARGET_BUILD_DIR}/${INFOPLIST_PATH}"

# DESCRIPTION should be sth like 0.0.4-1-gcad9f67b
CURRENT_TAG=`git tag -l --points-at HEAD`
DESCRIPTION=`git describe --long --first-parent --match "$VERSION_NUMBER"`

# Build number - CFBundleVersion

if [ $DESCRIPTION ]; then
  IFS="-" read -ra TOKENIZED <<< "$DESCRIPTION"
  BUILD_NUMBER="${TOKENIZED[1]}"
else
  BUILD_NUMBER=`git rev-list HEAD --count --first-parent`
fi
/usr/libexec/PlistBuddy -c \
"Set :CFBundleVersion $BUILD_NUMBER" "${TARGET_BUILD_DIR}/${INFOPLIST_PATH}"

# Logging
echo "Update bundle version: $VERSION_NUMBER.$BUILD_NUMBER"
