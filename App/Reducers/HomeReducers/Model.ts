
export enum HomeSnippetType {
  LoanCalculator,
  LoanInApplication,
  PromotionBanner,
}

export interface HomeSnippet {
  type: HomeSnippetType;
}

export interface LoanSnippet extends HomeSnippet {
  type: HomeSnippetType.LoanInApplication;
}

export interface BannerSnippet extends HomeSnippet {
  type: HomeSnippetType.PromotionBanner;
  key: string;
  description: string;
  image: string;
  link: string;
}
