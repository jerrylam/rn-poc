import { Action, Reducer } from "redux";
import { createActions, createReducer } from "reduxsauce";
import * as SI from "seamless-immutable";
import { BannerSnippet, HomeSnippet, HomeSnippetType, LoanSnippet } from "./Model";

/* ------------- Types and Action Creators ------------- */
export { BannerSnippet, HomeSnippet, HomeSnippetType, LoanSnippet };
const { Types, Creators } = createActions({
  goHome: ["fetchLatest"],
  refresh: ["fetchLatest"],

  // services/APIS/sagas
  onRefreshed: ["snippets"],
}, {
  prefix: "HOME/",
});

export const HomeTypes = Types;
export const HomeActions = Creators;

export interface HomeState {
  isRefreshing?: boolean;
  fetchLatest?: boolean;
  snippets?: HomeSnippet[];
  promotionBanners?: BannerSnippet[];
}

export type HomeAction = HomeState & Action;

export type ImmutableHomeState = SI.ImmutableObject<HomeState>;

export const INITIAL_STATE: ImmutableHomeState = SI.from({
  isRefreshing: false,
  snippets: [],
  promotionBanners: [],
});

export const refresh: Reducer<ImmutableHomeState> = (
  state = INITIAL_STATE,
) => {
  return state.merge({
    isRefreshing: true,
   });
};

export const onRefreshed: Reducer<ImmutableHomeState> = (
  state = INITIAL_STATE,
  action: HomeAction,
) => {
  const { snippets } = action;
  return state.merge({
    isRefreshing: false,
    snippets,
   });
};

export const HomeReducer = createReducer(INITIAL_STATE, {
  [Types.GO_HOME]: refresh,
  [Types.REFRESH]: refresh,
  [Types.ON_REFRESHED]: onRefreshed,
});

export default HomeReducer;
