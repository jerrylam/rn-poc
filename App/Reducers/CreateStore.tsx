import {
  createReactNavigationReduxMiddleware,
} from "react-navigation-redux-helpers";
import { applyMiddleware, compose, createStore, Reducer } from "redux";
import sagaMiddlewareFactory, { SagaIterator } from "redux-saga";
import { RootState } from ".";

// creates the store
export default (rootReducer: Reducer<any>, rootSaga: () => SagaIterator) => {
  /* ------------- Redux Configuration ------------- */

  const middleware = [];
  const enhancers = [];

  /* ------------- Analytics Middleware ------------- */

  /* ------------- Saga Middleware ------------- */

  const sagaMiddleware = sagaMiddlewareFactory();
  middleware.push(sagaMiddleware);

  /* ------------- React Navigation Middleware ------------- */
  const reactNavigationMiddleware = createReactNavigationReduxMiddleware(
    (state: RootState) => state.nav,
    "root",
  );
  middleware.push(reactNavigationMiddleware);

  /* ------------- Assemble Middleware ------------- */

  enhancers.push(applyMiddleware(...middleware));
  const composeEnhancers =
  (window && (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;

  const store = createStore(rootReducer, composeEnhancers(...enhancers));

  // kick off root saga
  const sagasManager = sagaMiddleware.run(rootSaga);

  return {
    store,
    sagasManager,
    sagaMiddleware,
  };
};
