import { AppStateStatus } from "react-native";
import { Action, Reducer } from "redux";
import { createActions, createReducer } from "reduxsauce";
import * as SI from "seamless-immutable";

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  setFirstVisit: ["firstVisit"],
  onAppStateChange: ["appStateStatus"],
  exitApp: null,
});

export const AppTypes = Types;
export const AppActions = Creators;

interface AppState {
  firstVisit?: boolean;
  appStateStatus?: AppStateStatus;
}

export type AppAction = AppState & Action;

export type ImmutableAppState = SI.ImmutableObject<AppState>;

/* ------------- Initial State ------------- */

export const INITIAL_STATE: ImmutableAppState = SI.from({
  firstVisit: true,
  fcmToken: null,
});

/* ------------- Reducers ------------- */

export const setFirstVisit: Reducer<ImmutableAppState> =
  (state = INITIAL_STATE, { firstVisit }: AppAction) => {
    return state.merge({ firstVisit });
};

/* ------------- Hookup Reducers To Types ------------- */

export const AppReducer = createReducer(INITIAL_STATE, {
  [Types.SET_FIRST_VISIT]: setFirstVisit,
});

export default AppReducer;
