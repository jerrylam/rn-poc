import { createNavigationReducer, createReduxContainer } from "react-navigation-redux-helpers";
import { persistCombineReducers, persistStore } from "redux-persist";
import { seamlessImmutableReconciler, seamlessImmutableTransformCreator } from "redux-persist-seamless-immutable";
import storage from "redux-persist/lib/storage";
import AppNavigation from "../Navigation/AppNavigation";
import root from "../Sagas";
import { AppReducer, ImmutableAppState } from "./AppReducers";
import configureStore from "./CreateStore";
import { HomeReducer, ImmutableHomeState } from "./HomeReducers";
import { NavigationState } from "./NavigationReducers";

/* ------------- Assemble The Reducers ------------- */
const transformerConfig = {
    whitelistPerReducer: {
    },
    blacklistPerReducer: {
      app: ["sanctionLocationDetected"],
    },
  };
const navReducer = createNavigationReducer(AppNavigation);
const persistConfig = {
  key: "root",
  timeout: 10000,
  storage,
  stateReconciler: seamlessImmutableReconciler,
  transforms: [seamlessImmutableTransformCreator(transformerConfig)],
  whitelist: ["app", "setting", "options", "applicationProgress"],
  blacklist: ["_persist"],
};

export const rootReducer = persistCombineReducers(persistConfig, {
  app: AppReducer,
  nav: navReducer,
  home: HomeReducer,
});

export interface RootState {
  nav: NavigationState;
  app: ImmutableAppState;
  home: ImmutableHomeState;
}

export default () => {
  const persistedReducer = rootReducer;
  // tslint:disable-next-line:prefer-const
  let { store, sagasManager, sagaMiddleware } = configureStore(persistedReducer, root);
  if (module.hot) {
    module.hot.accept("root", () => {
      const nextRootReducer = require("./").rootReducer;
      store.replaceReducer(nextRootReducer);
      const newYieldedSagas = require("../Sagas").default;
      sagasManager.cancel();
      sagasManager = sagaMiddleware.run(newYieldedSagas);
    });
  }
  const persistor = persistStore(store);
  const reduxContainer = createReduxContainer(AppNavigation, "root");
  return { store, persistor, reduxContainer };
};
