/// <reference types="@types/jest" />
import * as React from "react";
import { Provider } from "react-redux";
import * as renderer from "react-test-renderer";
import configureStore from "redux-mock-store";
import LaunchSplashScreen from "./LaunchSplashScreen";

test("renders correctly", () => {
  const mockStore = configureStore()({});
  const wrapper = renderer.create(<Provider store={mockStore}><LaunchSplashScreen /></Provider>);
  expect(wrapper).toMatchSnapshot();
});
