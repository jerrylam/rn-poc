import * as React from "react";
import { Image, StatusBar, Text, TextStyle, View } from "react-native";
import { connect } from "react-redux";
import * as Redux from "redux";
import { RootState } from "../../Reducers";
import { Images } from "../../Themes";

// Styles

/**
 * The properties passed to the component
 */
export interface OwnProps {
  instantName?: string;
}
/**
 * The properties mapped from Redux dispatch
 */
export interface DispatchProps {
  onNextScreen?: () => void;
}

/**
 * The properties mapped from the global state
 */
export interface StateProps {
  language?: string;
}

/**
 * The local state
 */
export interface State {

}

type Props = OwnProps & DispatchProps & StateProps;

export class LaunchSplashScreen extends React.Component<Props, State> {

  public render() {
    const textContainerStyle = {
      position: "absolute",
      justifyContent: "center",
      alignItems: "center",
      width: "100%",
      height: "100%",
    } as TextStyle;
    return (
      <View style={{ backgroundColor: "#124411", flex: 1}} >
        <StatusBar barStyle="dark-content" />
        <Image
          source={Images.launchScreens.splash.background_large}
          resizeMode="cover"
        />
        <View style={textContainerStyle}>
          <Text style={{color: "#ffffff"}}>Splash Screen</Text>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state: RootState): StateProps => ({
  language: "en",
});

const mapDispatchToProps = (dispatch: Redux.Dispatch< Redux.AnyAction>): DispatchProps => ({
  onNextScreen: () => {
    // TODO:
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(LaunchSplashScreen) as React.ComponentClass<OwnProps>;
