import * as React from "react";
import { connect, Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import createStore, { RootState } from "../Reducers";
import RootContainer from "./RootContainer";

/**
 * Provides an entry point into our application.  Both index.ios.js and index.android.js
 * call this component first.
 *
 * We create our Redux store here, put it into a provider and then bring in our
 * RootContainer.
 *
 * We separate like this to play nice with React Native's hot reloading.
 */

const mapStateToProps = (state: RootState) => ({ state: state.nav });

const createNavigationContainer = (reduxContainer: React.ComponentType<{
  state: any;
  dispatch: any;
}>) => {
  return connect(mapStateToProps)(reduxContainer);
};

const App = () => {
  // create our store
  const { store, persistor, reduxContainer } = createStore();
  const NavContainer = createNavigationContainer(reduxContainer);
  const navContainer = (
    <NavContainer/>
  );
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <RootContainer navigationContainer={navContainer}/>
      </PersistGate>
    </Provider>
  );
};

export default App;
