import moment, { Moment } from "moment";
import * as React from "react";
import {
  Alert,
  AlertOptions,
  AppState,
  BackHandler,
  InteractionManager,
  StatusBar,
  View,
} from "react-native";
import { connect } from "react-redux";
import * as Redux from "redux";
import { RootState } from "../../Reducers";
import { NavigationState } from "../../Reducers/NavigationReducers";
import { StartupActions } from "../../Reducers/StartupReducers";
/**
 * The properties mapped from Redux dispatch
 */
export interface DefaultProps {
  navigationContainer: React.ComponentType<{
    state: any;
    dispatch: any;
  }>;
}

export interface StateProps {
  nav: NavigationState;
  sanctionLocationDetected?: boolean;
}

export interface DispatchProps {
  startup: () => void;
  exitApp: () => void;
}

interface State {
  backTimestamp: Moment;
  sanctionLocationMaskShown: boolean;
}

type Props = StateProps & DispatchProps & DefaultProps;

export class RootContainer extends React.Component<Props, State> {
  private firebaseListeners: any;
  constructor(props: Props) {
    super(props);
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    this.state = {
      backTimestamp: moment(),
      sanctionLocationMaskShown: false,
    };
    this._onModalRequestClose = this._onModalRequestClose.bind(this);
  }

  public componentDidMount() {
    this.props.startup();
    BackHandler.addEventListener("hardwareBackPress", this.handleBackButtonClick);
  }

  public componentWillUnmount() {
    // UI component will dismiss but it doesn't mean Redux will be stopped/reset on Android platform.
    BackHandler.removeEventListener("hardwareBackPress", () => {
      // empty handler
    });
    AppState.removeEventListener("change", () => {
      // empty handler
    });
  }

  public render() {
    return (
      <View style={{ backgroundColor: "#23d211", flex: 1}}>
      <StatusBar barStyle="light-content" />
      {this.props.navigationContainer}
    </View>
    );
  }
  private handleBackButtonClick() {
    if (this.props.nav.index === 0) {
      const currentTimeStamp = moment();
      if (currentTimeStamp.diff(this.state.backTimestamp, "seconds", true) < 2) {
        this.props.exitApp();
      } else {
        InteractionManager.runAfterInteractions(() => {
          this.setState({
            backTimestamp: currentTimeStamp,
          });
          setTimeout(() => {
            Alert.alert(
              "Press again to exit",
              undefined,
              [],
              { cancelable: false} as AlertOptions,
            );
          }, 0);
        });

      }
    }
  }

  private _onModalRequestClose() {
    // do nth
  }
}

const mapStateToProps = (state: RootState): StateProps => {
  return {
    nav: state.nav,
  };
};

const mapDispatchToProps = (dispatch: Redux.Dispatch< Redux.AnyAction>): DispatchProps => ({
  startup: () => {
    dispatch(StartupActions.startup());
  },
  exitApp: () => {
    // TODO
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(RootContainer);
