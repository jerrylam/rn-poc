import { Alert, InteractionManager } from "react-native";
import { NavigationActions, StackActions } from "react-navigation";
import { SagaIterator } from "redux-saga";
import { call, fork, put, take } from "redux-saga/effects";
import { HomeAction, HomeActions, HomeSnippet, HomeSnippetType, HomeTypes } from "../../Reducers/HomeReducers";

export function* goHome(action: HomeAction): SagaIterator {
  const { fetchLatest } = action;
  yield put(StackActions.reset({
    index: 0,
    key: null,
    actions: [
      NavigationActions.navigate({ routeName: "Home" }),
    ],
  }));
  yield call(renderScreenSnippets, fetchLatest);
  yield fork(listenForRefresh);
}

export function* listenForRefresh(): SagaIterator {
  while (true) {
    const { fetchLatest } = yield take(HomeTypes.REFRESH);
    yield call(renderScreenSnippets, !!(fetchLatest));
  }
}

export function* renderScreenSnippets(fetchLatest?: boolean): SagaIterator {
  if (fetchLatest) {
    yield call(fetchBanners);
  }
  yield call(loadSnippets);
}

export function* loadSnippets(): SagaIterator {
  const homeSnippets = [] as HomeSnippet[];
  homeSnippets.push({
    type: HomeSnippetType.LoanCalculator,
  });
  const sortedSnippets = yield call(sortSnippets, homeSnippets);
  yield put(HomeActions.onRefreshed(sortedSnippets));
}

export function* sortSnippets(initialSnippets: HomeSnippet[]): SagaIterator {
  return initialSnippets.sort((a, b) => {
    if (a.type === b.type) {
      return 0;
    } else if (a.type === HomeSnippetType.PromotionBanner) {
      if (b.type === HomeSnippetType.LoanCalculator) {
        return -1;
      } else {
        return 1;
      }
    } else if (a.type === HomeSnippetType.LoanCalculator) {
      if (b.type === HomeSnippetType.PromotionBanner) {
        return 1;
      } else {
        return -1;
      }
    } else if (a.type === HomeSnippetType.LoanInApplication) {
      if (b.type === HomeSnippetType.PromotionBanner) {
        return -1;
      } else {
        return 1;
      }
    }
    return 0;
  });
}

export function* fetchBanners(): SagaIterator {
  // TODO
}

export function _alertGeneralError() {
  InteractionManager.runAfterInteractions(() => {
    setTimeout(() => {
      Alert.alert(
        "errors.generic.title",
       "errors.generic.message",
        [{ text: "options.ok" }],
        { cancelable: false },
      );
    }, 0);
  });
}
