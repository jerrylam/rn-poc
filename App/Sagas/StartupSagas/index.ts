import NetInfo from "@react-native-community/netinfo";
import { Action } from "redux";
import { SagaIterator } from "redux-saga";
import { all, call, cancel, delay, put, race, select, take } from "redux-saga/effects";
import { RootState } from "../../Reducers";
import { HomeActions } from "../../Reducers/HomeReducers";
import { StartupTypes } from "../../Reducers/StartupReducers";

// exported to make available for tests

// exported to make available for tests
export function getIsFirstVisit(state: RootState): boolean {
  if (state.app.firstVisit === undefined) {
    return true;
  } else {
    return state.app.firstVisit;
  }
}

//
// Start up flow for authenticating TouchID / FaceID / Fingerprint on launch
//

// exported to make available for tests
export function* _navigateToLandingScreen(): SagaIterator {
  yield delay(2000);
  const isFirstVisit: boolean = yield select(getIsFirstVisit);
  yield put(HomeActions.goHome(true));
}

//
// process STARTUP actions
//
export function* startup(action: Action): SagaIterator {
  // magic code to make NetInfo.isConnected return correct value on iOS.
  yield call(NetInfo.isConnected.addEventListener, "connectionChange", (newState: any) => {
      "Network changed";
  });
  const watcher = yield all([
    call(_navigateToLandingScreen),
  ]);
  yield take(StartupTypes.STARTUP);
  yield cancel(watcher);
}
