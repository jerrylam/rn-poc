import { all, takeLatest } from "redux-saga/effects";
/* ------------- Types ------------- */
import { AppTypes } from "../Reducers/AppReducers";
import { StartupTypes } from "../Reducers/StartupReducers";

/* ------------- Sagas ------------- */
import { HomeTypes } from "../Reducers/HomeReducers";
import { exitApp } from "../Services/Device/Actions";
import { goHome } from "./HomeSagas";
import { startup } from "./StartupSagas";

/* ------------- Connect Types To Sagas ------------- */

export default function* root() {
  yield all([
    // some sagas only receive an action
    takeLatest(StartupTypes.STARTUP, startup),
    takeLatest(HomeTypes.GO_HOME, goHome),
    takeLatest(AppTypes.EXIT_APP, exitApp),
  ]);
}
