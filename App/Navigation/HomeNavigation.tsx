import {
  createStackNavigator,
} from "react-navigation";
import HomeScreen from "../Containers/HomeScreen";

const HomeNavigation = createStackNavigator({HomeScreen: { screen: HomeScreen }}, {
  // Default config for all screens
  headerMode: "none",
  initialRouteName: "HomeScreen",
});

export default HomeNavigation;
