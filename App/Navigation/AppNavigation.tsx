import { Platform } from "react-native";
import { createStackNavigator } from "react-navigation";
import HomeNavigation from "./HomeNavigation";
import LaunchScreensNavigation from "./LaunchScreensNavigation";

const AppNavigation = createStackNavigator({
  Home: { screen: HomeNavigation },
  LaunchScreens: { screen: LaunchScreensNavigation },
}, {
  // Default config for all screens
  initialRouteName: "LaunchScreens",
  headerMode: "none",
  mode: Platform.OS === "ios" ? "modal" : "card",
  navigationOptions: {
    gesturesEnabled: false,
  },
});

export default AppNavigation;
