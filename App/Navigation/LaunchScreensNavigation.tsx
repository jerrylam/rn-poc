import { createStackNavigator } from "react-navigation";
import LaunchSplashScreen from "../Containers/LaunchSplashScreen";

const LaunchScreensNavigation = createStackNavigator(
  {
    SplashScreen: {
      screen: LaunchSplashScreen,
    },
  }, {
  // Default config for all screens
  initialRouteName: "SplashScreen",
  headerMode: "none",
});

export default LaunchScreensNavigation;
